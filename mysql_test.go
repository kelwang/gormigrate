//go:build mysql
// +build mysql

package gormigrate

import (
	_ "gitlab.com/kelwang/gorm/dialects/mysql"
)

func init() {
	databases = append(databases, database{
		name:    "mysql",
		connEnv: "MYSQL_CONN_STRING",
	})
}

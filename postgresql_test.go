//go:build postgresql
// +build postgresql

package gormigrate

import (
	_ "gitlab.com/kelwang/gorm/dialects/postgres"
)

func init() {
	databases = append(databases, database{
		name:    "postgres",
		connEnv: "PG_CONN_STRING",
	})
}

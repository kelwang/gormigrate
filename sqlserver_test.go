//go:build sqlserver
// +build sqlserver

package gormigrate

import (
	_ "gitlab.com/kelwang/gorm/dialects/mssql"
)

func init() {
	databases = append(databases, database{
		name:    "mssql",
		connEnv: "SQLSERVER_CONN_STRING",
	})
}

//go:build sqlite
// +build sqlite

package gormigrate

import (
	_ "gitlab.com/kelwang/gorm/dialects/sqlite"
)

func init() {
	databases = append(databases, database{
		name:    "sqlite",
		connEnv: "SQLITE_CONN_STRING",
	})
}
